import com.devcamp.task54b_80.Circle;
import com.devcamp.task54b_80.Rectangle;
import com.devcamp.task54b_80.Shape;
import com.devcamp.task54b_80.Square;

public class App {
    public static void main(String[] args) throws Exception {
        // Test 6
        Shape shape1 = new Shape();
        Shape shape2 = new Shape(false, "green");
        System.out.println(shape1.toString());
        System.out.println(shape2.toString());

        // Test 7
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, true, "green");
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());

        // Test 8
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle(2.0, 1.5, true, "green");
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());
        System.out.println(rectangle3.toString());

        // Test 9
        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square(2.0, true, "green");
        System.out.println(square1.toString());
        System.out.println(square2.toString());
        System.out.println(square3.toString());

        // Test 10
        System.out.println("Circle 1 area: " + circle1.getArea());
        System.out.println("Circle 1 perimeter: " + circle1.getPerimeter());
        System.out.println("Circle 2 area: " + circle2.getArea());
        System.out.println("Circle 2 perimeter: " + circle2.getPerimeter());
        System.out.println("Circle 3 area: " + circle3.getArea());
        System.out.println("Circle 3 perimeter: " + circle3.getPerimeter());

        // Test 11
        System.out.println("Rectangle 1 area: " + rectangle1.getArea());
        System.out.println("Rectangle 1 perimeter: " + rectangle1.getPerimeter());
        System.out.println("Rectangle 2 area: " + rectangle2.getArea());
        System.out.println("Rectangle 2 perimeter: " + rectangle2.getPerimeter());
        System.out.println("Rectangle 3 area: " + rectangle3.getArea());
        System.out.println("Rectangle 3 perimeter: " + rectangle3.getPerimeter());

        // Test 12
        System.out.println("Square 1 area: " + square1.getArea());
        System.out.println("Square 1 perimeter: " + square1.getPerimeter());
        System.out.println("Square 2 area: " + square2.getArea());
        System.out.println("Square 2 perimeter: " + square2.getPerimeter());
        System.out.println("Square 3 area: " + square3.getArea());
        System.out.println("Square 3 perimeter: " + square3.getPerimeter());
    }
}
