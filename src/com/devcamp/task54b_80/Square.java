package com.devcamp.task54b_80;

public class Square extends Rectangle {
    public Square() {
        this(1.0);
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(double side, boolean filled, String color) {
        super(side, side, filled, color);
    }

    public double getSide() {
        return getWidth();
    }

    public void setSide(double side) {
        setWidth(side);
        setLength(side);
    }

    public void setWidth(double width) {
        setSide(width);
    }

    public void setLength(double length) {
        setSide(length);
    }

    @Override
    public String toString() {
        return "Square[" + super.toString() + "]";
    }

}
